window.config = {
   observbleUrls: [
      '*://*.telemetryverification.net/*',
      '*://*.vindicosuite.com/*',
      '*://*.tidaltv.com/*',
      '*://*.mookie1.com/*',
      '*://*.doubleclick.net/*',
      '*://rtr.innovid.com/r1*',
      '*://*.freewheel.tv/*',
      '*://optimized-by.rubiconproject.com/a/api/vast*',
      '*://uswvideo.adsrvr.org/data/vast/*',
      '*://*.liverail.com/*',
      '*://ads.adaptv.advertising.com/*',
      '*://search.spotxchange.com/vast/*',
      '*://vpc.altitude-arena.com/*',
      '*://*.tubemogul.com/*',
      '*://vast.yashi.com/*',
      '*://dbam.dashbida.com/kitaramedia/vast/*',
      '*://app.scanscout.com/*',
      '*://*.amazon-adsystem.com/*',
      '*://ad.lkqd.net/*',
      '*://*.spotxchange.com/*'
   ]
};

